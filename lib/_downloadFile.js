/**
 * Download File
 */
const downloadFile = async function (_bfpUri, _progressCallback) {
    /* Initialize chunks. */
    let chunks = []

    /* Initialize size. */
    let size = 0

    /* Initialize txid. */
    let txid = _bfpUri

    /* Filter txid. */
    txid = txid.replace('bitcoinfile:', '')
    txid = txid.replace('bitcoinfiles:', '')

    /* Request transaction. */
    const tx = await this.bitbox.Transaction.details(txid)
        .catch(err => console.error(err))
    // console.log('\nTX', tx)

    /* Set previous hash. */
    let prevHash = tx.vin[0].txid
    console.log('\nprevHash', prevHash)

    /* Set metadata OP_RETURN (hex). */
    const metadata_opreturn_hex = tx.vout[0].scriptPubKey.hex
    console.log('\nmetadata_opreturn_hex', metadata_opreturn_hex)

    const bfpMsg = require('./_parseDataOpReturn')
        .bind(this)(metadata_opreturn_hex)
    console.log('\nbfpMsg', bfpMsg)

    /* Set file name. */
    const filename = bfpMsg.filename

    /* Set file extension. */
    const fileext = bfpMsg.fileext

    /* Set file size. */
    const filesize = bfpMsg.filesize

    /* Set SHA256 hash. */
    const sha256 = bfpMsg.sha256

    /* Set download count. */
    let downloadCount = bfpMsg.chunk_count
    console.log('\ndownloadCount', downloadCount)

    /* Validate BFP message chunk count. */
    if (bfpMsg.chunk_count > 0 && bfpMsg.chunk !== null) {
        /* Set download count. */
        downloadCount = bfpMsg.chunk_count - 1

        /* Add chunk. */
        chunks.push(bfpMsg.chunk)

        /* Add chunk length to size. */
        size += bfpMsg.chunk.length
    }

    /* Loop through raw transactions, parse out data. */
    for (let index = 0; index < downloadCount; index++) {
        /* Request transaction details. */
        const tx = await this.bitbox.Transaction.details(prevHash)
            .catch(err => console.error(err))
        // console.log('\nTX (prev)', index, tx)

        /* Set previous hash. */
        prevHash = tx.vin[0].txid

        /* Set OP_RETURN (hex). */
        const op_return_hex = tx.vout[0].scriptPubKey.hex
        console.log('\nop_return_hex (prev)', index, op_return_hex)

        /* Parse message. */
        const bfpMsg = require('./_parseDataOpReturn').bind(this)(op_return_hex)
        console.log('\nbfpMsg (prev)', index, bfpMsg)

        /* Add chunk. */
        chunks.push(bfpMsg.chunk)

        /* Add length. */
        size += bfpMsg.chunk.length

        /* Handle callback. */
        if (_progressCallback) {
            _progressCallback(index / (downloadCount - 1))
        }
    }

    /* Reverse order of chunks. */
    chunks = chunks.reverse()

    /* Initialize file buffer. */
    let fileBuf = Buffer.alloc(size)

    /* Initilize index. */
    let index = 0

    /* Handle chunks. */
    chunks.forEach(chunk => {
        /* Copy chunk. */
        chunk.copy(fileBuf, index)

        /* Add chunk length to index. */
        index += chunk.length
    })

    // TODO: check that metadata hash matches if one was provided.
    let passesHashCheck = false

    /* Validate message hash. */
    if (bfpMsg.sha256 !== null) {
        /* Calculate file hash. */
        const fileSha256 = this.bitbox.Crypto.sha256(fileBuf)

        /* Calculate result. */
        const res = Buffer.compare(fileSha256, bfpMsg.sha256)

        /* Validate result. */
        if (res === 0) {
            /* Set passed hash check flag. */
            passesHashCheck = true
        }
    }

    /* Build results. */
    const results = {
        passesHashCheck,
        fileBuf,
        filename,
        fileext,
        filesize,
        sha256,
    }

    /* Return results. */
    return results
}

/* Export module. */
module.exports = downloadFile
