/* Import module. */
const BITBOX = require('bitbox-sdk').BITBOX
const bitbox = new BITBOX()
const debug = require('debug')('bitcoinfilesjs-utils')

/* Set BFP namespace. */
const BML_NAMESPACE = '424D4C00' // BML

/**
 * Integer to Hex
 */
function _intToBuffer(_int) {
    /* Calculate string. */
    const str = Number(_int).toString(16)

    /* Calculate hex. */
    const hex = (str.length % 2) === 1 ? '0' + str : str

    /* Return hex. */
    return Buffer.from(hex, 'hex')
}

/**
 * Bitcoin Files Protocol (BFP) Utilities
 */
class BFPUtils {
    /**
     * Get Push Data Opcode
     */
    static getPushDataOpcode(_data) {
        debug('getPushDataOpcode (data):', _data)

        /* Set length. */
        const length = _data.length

        /* Handle length. */
        if (length === 0) {
            return [0x4c, 0x00]
        } else if (length < 76) {
            return length
        } else if (length < 256) {
            return [0x4c, length]
        } else {
            throw Error('Pushdata too large')
        }
    }

    /**
     * Integer to Fixed Buffer
     */
    static int2FixedBuffer(_amount, _size) {
        debug('int2FixedBuffer (amount):', _amount)
        debug('int2FixedBuffer (size):', _size)

        /* Set hex. */
        let hex = _amount.toString(16)

        /* Pad hex start. */
        hex = hex.padStart(_size * 2, '0')

        /* Handle odd padding. */
        if (hex.length % 2) hex = '0' + hex

        /* Return hex. */
        return Buffer.from(hex, 'hex')
    }

    /**
     * Encode Script
     */
    static encodeScript(_script) {
        // debug('encodeScript (script):', _script)

        /* Calculate buffer size. */
        const bufferSize = _script.reduce((acc, cur) => {
            if (Array.isArray(cur)) {
                return acc + cur.length
            } else {
                return acc + 1
            }
        }, 0)

        /* Set buffer. */
        const buffer = Buffer.allocUnsafe(bufferSize)

        /* Initialize offset. */
        let offset = 0

        /* Handle script items. */
        _script.forEach((scriptItem) => {
            if (Array.isArray(scriptItem)) {
                scriptItem.forEach((item) => {
                    buffer.writeUInt8(item, offset)

                    offset += 1
                })
            } else {
                buffer.writeUInt8(scriptItem, offset)

                offset += 1
            }
        })

        /* Return buffer. */
        return buffer
    }

    static calculateFileUploadCost(fileSizeBytes, configMetadataOpReturn, fee_rate = 1){
        let byte_count = fileSizeBytes
        let whole_chunks_count = Math.floor(fileSizeBytes / 220)
        let last_chunk_size = fileSizeBytes % 220

        // cost of final transaction's op_return w/o any chunkdata
        let final_op_return_no_chunk = this.buildMetadataOpReturn(configMetadataOpReturn)
        byte_count += final_op_return_no_chunk.length

        // cost of final transaction's input/outputs
        byte_count += 35
        byte_count += 148 + 1

        // cost of chunk trasnsaction op_returns
        byte_count += (whole_chunks_count + 1) * 3

        if (!this.chunk_can_fit_in_final_opreturn(final_op_return_no_chunk.length, last_chunk_size)) {
            // add fees for an extra chunk transaction input/output
            byte_count += 149 + 35
            // opcode cost for chunk op_return
            byte_count += 16
        }

        // output p2pkh
        byte_count += 35 * (whole_chunks_count)

        // dust input bytes (this is the initial payment for the file upload)
        byte_count += (148 + 1) * whole_chunks_count

        // other unaccounted per txn
        byte_count += 22 * (whole_chunks_count + 1)

        // dust output to be passed along each txn
        let dust_amount = 546

        return byte_count * fee_rate + dust_amount
    }

    static chunk_can_fit_in_final_opreturn (script_length, chunk_data_length) {
        if (chunk_data_length === 0) {
            return true
        }

        let op_return_capacity = 223 - script_length;
        if (op_return_capacity >= chunk_data_length) {
            return true
        }

        return false
    }

    /**
     * Get File Upload Payment Info from Seed Phrase
     */
    static getFileUploadPaymentInfoFromSeedPhrase(_mnemonic) {
        const seedBuffer = bitbox.Mnemonic.toSeed(_mnemonic)

        // create HDNode from seed buffer
        const hdNode = bitbox.HDNode.fromSeed(seedBuffer)
        const hdNode2 = bitbox.HDNode.derivePath(hdNode, `m/44'/145'/0'`)

        const node = bitbox.HDNode.derivePath(hdNode2, '0/0')

        const keyPair = bitbox.HDNode.toKeyPair(node)

        const wif = bitbox.ECPair.toWIF(keyPair)

        const ecPair = bitbox.ECPair.fromWIF(wif)

        const cashAddress = bitbox.ECPair.toCashAddress(ecPair)
        const legacyAddress = bitbox.Address.toLegacyAddress(cashAddress)

        return {
            cashAddress,
            legacyAddress,
            wif,
        }
    }

    // static getFileUploadPaymentInfoFromHdNode(masterHdNode) {
    //     let hdNode = bitbox.HDNode.derivePath(masterHdNode, "m/44'/145'/1'");
    //     let node0 = bitbox.HDNode.derivePath(hdNode, '0/0');
    //     let keyPair = bitbox.HDNode.toKeyPair(node0);
    //     let wif = bitbox.ECPair.toWIF(keyPair);
    //     let ecPair = bitbox.ECPair.fromWIF(wif);
    //     let address = bitbox.ECPair.toLegacyAddress(ecPair);
    //     let cashAddress = bitbox.Address.toCashAddress(address);

    //     return {address: cashAddress, wif: wif};
    // }

    /**
     * Build Metadata OP_RETURN
     */
    static buildMetadataOpReturn(_config) {
        // console.log('\nbuildMetadataOpReturn (config):', _config)

        /* Initialize script parts. */
        const scriptParts = []

        /* Add message type. */
        scriptParts.push(_intToBuffer(_config.msgType))

        /* Add chunk count. */
        if (_config.chunkCount === 0) {
            scriptParts.push(0)
        } else {
            debug('_config.chunkCount', _config.chunkCount)
            scriptParts.push(_intToBuffer(_config.chunkCount))
        }

        /* Add file name. */
        if (_config.fileName === null || _config.fileName.length === 0 || _config.fileName === '') {
            scriptParts.push(0)
        } else {
            const buf = Buffer.from(_config.fileName, 'utf8')
            scriptParts.push(buf)
        }

        /* Add file extension. */
        if (_config.fileExt === null || _config.fileExt.length === 0 || _config.fileExt === '') {
            scriptParts.push(0)
        } else {
            const buf = Buffer.from(_config.fileExt, 'utf8')
            scriptParts.push(buf)
        }

        /* Set file size. */
        const fileSize = _intToBuffer(_config.fileSize)
        scriptParts.push(fileSize)

        /* Initialize regular expression. */
        // NOTE: File SHA256
        const re = /^[0-9a-fA-F]+$/

        /* Validate file version SHA256. */
        if (_config.fileSha256Hex === null || _config.fileSha256Hex.length === 0 || _config.fileSha256Hex === '') {
            scriptParts.push(0)
        } else if (_config.fileSha256Hex.length === 64 && re.test(_config.fileSha256Hex)) {
            const fileSha256Buf = Buffer.from(_config.fileSha256Hex, 'hex')
            scriptParts.push(fileSha256Buf)
        } else {
            throw Error('File hash must be provided as a 64 character hex string')
        }

        /* Validate previous file version SHA256. */
        if (_config.prevFileSha256Hex === null || _config.prevFileSha256Hex.length === 0 || _config.prevFileSha256Hex === '') {
            scriptParts.push(0)
        } else if (_config.prevFileSha256Hex.length === 64 && re.test(_config.prevFileSha256Hex)) {
            const prevFileSha256Hex = Buffer.from(_config.prevFileSha256Hex, 'hex')
            scriptParts.push(prevFileSha256Hex)
        } else {
            throw Error('Previous File hash must be provided as a 64 character hex string')
        }

        /* Validate file URI. */
        if (_config.fileUri === null || _config.fileUri.length === 0 || _config.fileUri === '') {
            scriptParts.push(0)
        } else {
            const fileUri = Buffer.from(_config.fileUri)
            scriptParts.push(fileUri)
        }

        /* Validate chunk data. */
        if (typeof _config.chunkData === 'undefined' || _config.chunkData === null || _config.chunkData.length === 0) {
            scriptParts.push(0)
        } else {
            const chunkData = Buffer.from(_config.chunkData, 'hex')
            scriptParts.push(chunkData)
        }

        /* Set encoded script. */
        const encodedScript = bitbox.Script.encode([
            bitbox.Script.opcodes.OP_RETURN,
            Buffer.from(BML_NAMESPACE, 'hex'),
            scriptParts[0], // msgType
            scriptParts[1], // chunkCount
            scriptParts[2], // fileName
            scriptParts[3], // fileExt
            scriptParts[4], // fileSize
            scriptParts[5], // fileSha256Hex
            scriptParts[6], // prevFileSha256Hex
            scriptParts[7], // fileUri
            scriptParts[8], // chunkData
        ])
        console.log('\nencodedScript', encodedScript)

        /* Validate script length. */
        if (encodedScript.length > 223) {
            throw Error('Script too long, must be less than 223 bytes.')
        }

        /* Return encoded script. */
        return encodedScript
    }

    /**
     * Build Data Chunk OP_RETURN
     */
    static buildDataChunkOpReturn(_chunkData) {
        // console.log('\nbuildDataChunkOpReturn (chunkData):', _chunkData)

        /* Initialize script. */
        let script = null

        /* Validate chunk data. */
        if (typeof _chunkData === 'undefined' || _chunkData === null || _chunkData.length === 0) {
            script = 0
        } else {
            script = _chunkData
        }

        // let encodedScript = this.encodeScript(script)

        /* Set encoded script. */
        const encodedScript = bitbox.Script.encode([
            bitbox.Script.opcodes.OP_RETURN,
            script, // chunkData
        ])
        console.log('\nencodedScript', encodedScript)

        /* Validate script length. */
        if (encodedScript.length > 223) {
            throw Error('Script too long, must be less than 223 bytes.')
        }

        /* Return encoded script. */
        return encodedScript
    }

    /**
     * Calculate Metadata Miner Fee
     */
    static calculateMetadataMinerFee(_genesisOpReturnLength, _feeRate = 1) {
        let fee = bitbox.BitcoinCash.getByteCount({ P2PKH: 1 }, { P2PKH: 1 })

        fee += _genesisOpReturnLength
        fee += 10 // added to account for OP_RETURN ammount of 0000000000000000
        fee *= _feeRate

        /* Return fee. */
        return fee
    }

    /**
     * Calculate Data Chunk Miner Fee
     */
    calculateDataChunkMinerFee(sendOpReturnLength, feeRate = 1) {
        let fee = bitbox.BitcoinCash.getByteCount({ P2PKH: 1 }, { P2PKH: 1 })

        fee += sendOpReturnLength
        fee += 10 // added to account for OP_RETURN ammount of 0000000000000000
        fee *= feeRate

        return fee
    }

}

/* Export module. */
module.exports = BFPUtils
