/* Set Bitcoin Markup Language (BML) namespace. */
const BML_NAMESPACE = '424D4C00' // BML

/**
 * Parse Data OP_RETURN
 */
const parseDataOpReturn = function (_hex) {
    /* Handle script. */
    const script = this.bitbox.Script.toASM(Buffer.from(_hex, 'hex')).split(' ')
    // console.log('\nparseDataOpReturn (script):', script)

    /* Initialize BFP data. */
    const bfpData = {}

    /* Set type. */
    bfpData.type = 'metadata'

    /* Validate script length. */
    if (script.length === 2) {
        /* Set type. */
        bfpData.type = 'chunk'

        try {
            /* Set chunk. */
            bfpData.chunk = Buffer.from(script[1], 'hex')
        } catch (e) {
            bfpData.chunk = null
        }

        /* Return BFP data. */
        return bfpData
    }

    /* Validate script index #0. */
    if (script[0] !== 'OP_RETURN') {
        throw new Error('Not an OP_RETURN')
    }

    /* Validate script index #1. */
    if (script[1].toUpperCase() !== BML_NAMESPACE) {
        throw new Error('Not a BML OP_RETURN')
    }

    /* Validate script index #2. */
    // NOTE: 01 = On-chain File
    // NOTE: bitcoincashlib-js converts hex 01 to OP_1 due to BIP62.3 enforcement
    if (script[2] !== 'OP_1') {
        throw new Error('Not a BFP file (type 0x01)')
    }

    /* Set chunk count. */
    bfpData.chunk_count = parseInt(script[3], 16)

    /* Validate script index #3. */
    if (script[3].includes('OP_')) {
        /* Filter value. */
        const val = script[3].replace('OP_', '')

        /* Set chunk count. */
        bfpData.chunk_count = parseInt(val)
    }

    /* Validate script index #4. */
    // NOTE: file name
    if (script[4] === 'OP_0') {
        bfpData.filename = null
    } else {
        bfpData.filename = Buffer.from(script[4], 'hex').toString('utf8')
    }

    /* Validate script index #5. */
    // NOTE: file extension (incl '.' prefix)
    if (script[5] === 'OP_0') {
        bfpData.fileext = null
    } else {
        bfpData.fileext = Buffer.from(script[5], 'hex').toString('utf8')
    }

    /* Validate script index #6. */
    // NOTE: file size
    if (script[6] === 'OP_0') {
        bfpData.filesize = null
    } else {
        bfpData.filesize = parseInt(script[6], 16)
    }

    /* Validate script index #7. */
    // NOTE: file sha256
    if (script[7] === 'OP_0') {
        bfpData.sha256 = null
    } else {
        bfpData.sha256 = Buffer.from(script[7], 'hex')
    }

    /* Validate script index #8. */
    // NOTE: previous file sha256
    if (script[8] === 'OP_0') {
        bfpData.prevsha256 = null
    } else {
        bfpData.prevsha256 = Buffer.from(script[8], 'hex')
    }

    /* Validate script index #9. */
    // NOTE: uri
    if (script[9] === 'OP_0') {
        bfpData.uri = null
    } else {
        bfpData.uri = Buffer.from(script[9], 'hex').toString('utf8')
    }

    /* Validate script index #10. */
    // NOTE: chunk_data
    if (script[10] === 'OP_0') {
        bfpData.chunk = null
    } else {
        try {
            bfpData.chunk = Buffer.from(script[10], 'hex')
        } catch(e) {
            bfpData.chunk = null
        }
    }

    /* Return BFP data. */
    return bfpData
}

/* Export module. */
module.exports = parseDataOpReturn
